#!/bin/bash
. setup/function.sh

function nginx_install {
    echo | apt-get install software-properties-common
    echo | add-apt-repository universe
    echo | add-apt-repository ppa:certbot/certbot
    echo | apt-get update

    install_package nginx
    install_package certbot
    install_package python-certbot-nginx
}
nginx_install