#!/bin/bash
. setup/function.sh

function python_install {
    # $1 projects path
    # $2 djange app path
    # $3 virtual env path
    echo | add-apt-repository ppa:jonathonf/python-3.6
    echo | apt-get update

    install_package python3.6
    install_package python3-pip
    pip3 install virtualenv
}

python_install
