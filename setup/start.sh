#!/bin/bash
IFS=$'\n\t'

declare -r DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Put all output to logfile
exec 3>&1 1>>${FILE_LOG} 2>&1

# update system
apt-get -qq update

. $DIR/functions.sh
. $DIR/python.sh
. $DIR/nginx.sh
. $DIR/postgres.sh
