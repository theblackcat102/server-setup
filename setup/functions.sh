#!/bin/bash

install_package() {
    yes | apt-get install "$@"
}