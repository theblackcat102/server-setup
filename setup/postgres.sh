#!/bin/bash

function postgresql_install {
    install_package postgresql postgresql-contrib
    systemctl start postgresql
}
postgresql_install
